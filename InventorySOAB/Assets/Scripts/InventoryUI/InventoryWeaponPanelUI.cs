using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using InventoryItems;

/// <summary>
/// Component for the WepoanPanel on the inventory,
/// to show the info of the weapon in use.
/// </summary>
public class InventoryWeaponPanelUI : MonoBehaviour {

    [SerializeField] private Text m_WeaponNameText, m_WeaponDPSText;
    [SerializeField] private Image m_ItemImagePanel;

    private InventorySlotUI m_SlotFromWeapon;
    private Color m_StartingColor;


    private void Awake() {
        m_StartingColor = m_ItemImagePanel.color;
    }


    public void EquipWeapon(InventorySlotUI slot) {
        m_WeaponNameText.text = slot.ItemNameText;
        m_WeaponDPSText.text = (slot.InventoryItem as InventoryWeapon).DamagePerSecond + "";
        m_ItemImagePanel.sprite = slot.ItemImage.sprite;
        m_SlotFromWeapon = slot;
    }

    public void RemoveEquipedWeapon() {
        m_WeaponNameText.text = "";
        m_WeaponDPSText.text = "";
        m_ItemImagePanel.sprite = null;
        m_ItemImagePanel.color = m_StartingColor;
    }

    /// <summary>
    /// Removes the info only if it comes from the same slot given as parameter.
    /// </summary>
    /// <param name="slot"></param>
    public void RemoveEquipedWeaponIfSlot(InventorySlotUI slot) {
        if (slot.Equals(m_SlotFromWeapon)) {
            RemoveEquipedWeapon();
        }
    }

}
