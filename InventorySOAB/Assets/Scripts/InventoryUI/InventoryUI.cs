using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using InventoryItems;

/// <summary>
/// Component attached to the UI component of the inventory.
/// It shows the information of the items from the inventory component
/// and allows the user to manage them.
/// </summary>
public class InventoryUI : MonoBehaviour {

    // There is only one, so a static instance is enough.
    // An instance in the world would be needed for more than one inventory.
    private static InventoryUI m_Instance;

    [SerializeField] private Text m_WeightText, m_MaxWeightText, m_MoneyText;
    // Transform holding the items' slots.
    [SerializeField] private Transform m_ItemsPanel; 
    [SerializeField] private InventoryWeaponPanelUI m_WeaponPanelUI;
    [SerializeField] private InventoryInfoPanelUI m_InfoItemPanel;
    [SerializeField] private GameObject m_SlotPrefab;

    private GameObject m_InventoryGO;
    private List<InventorySlotUI> m_SlotsList;

    private Inventory m_Inventory;
    public static Inventory Inventory 
        { get => m_Instance.m_Inventory; set => m_Instance.m_Inventory = value; }


    private void Awake() {
        if (m_Instance) {
            Destroy(m_Instance);
            m_Instance = null;
        }
        m_Instance = this;
        //
        m_InventoryGO = gameObject;
        m_SlotsList = new List<InventorySlotUI>();
        foreach (Transform child in m_ItemsPanel) {
            m_SlotsList.Add(child.GetComponent<InventorySlotUI>());
        }
    }

    private void Start() {
        m_WeightText.text = m_Inventory.Weight.ToString("f0"); ;
        m_MaxWeightText.text = m_Inventory.MaxWeight.ToString("f0");
        gameObject.SetActive(false);
    }


    public static void ShowOrHide(List<InventoryItem> items) {
        if (!m_Instance.m_InventoryGO.activeSelf) {
            // Update if it is being shown.
            UpdateItems(items);
        }
        m_Instance.m_InventoryGO.SetActive(!m_Instance.m_InventoryGO.activeSelf);
    }

    public static void UpdateItems(List<InventoryItem> items) {
        m_Instance.InstanceUpdateItems(items);
    }


    /// <summary>
    /// Method to update all the information of the inventory UI
    /// </summary>
    /// <param name="items"></param>
    private void InstanceUpdateItems(List<InventoryItem> items) {
        // Update slots needed.
        SetNeededSlotsAmount(items);
        // Update the slot info on the InfoPanel
        m_InfoItemPanel.UpdateSlotInfo();
        for (int i = 0; i < items.Count; i++) {
            InventoryItem item = items[i];
            InventorySlotUI slot = m_SlotsList[i];
            UpdateSlotInfo(slot, item);
        }
        // Update the wieght and the money.
        m_WeightText.text = m_Inventory.Weight.ToString("f1");
        m_MoneyText.text = m_Inventory.Money.ToString("f1");
    }

    /// <summary>
    /// It creates (or removes) as many as slots needed 
    /// in order to match the amount of inventory items.
    /// </summary>
    /// <param name="items"></param>
    private void SetNeededSlotsAmount(List<InventoryItem> items) {
        int diffAmount = items.Count - m_SlotsList.Count;
        if (diffAmount > 0) {
            // More items on the inventory than slots -> create more slots.
            for (int i = 0; i < diffAmount; i++) {
                GameObject slot = Instantiate(m_SlotPrefab, m_ItemsPanel);
                m_SlotsList.Add(slot.GetComponent<InventorySlotUI>());
            }
        } else if (diffAmount < 0) {
            // More slots than items on the inventory -> Remove unnecessary slots.
            for (int i = m_SlotsList.Count; i > items.Count; i--) {
                m_SlotsList[i - 1].gameObject.SetActive(false);
            }
        }
    }

    /// <summary>
    /// Update the slot info with the corresponding item from the inventory.
    /// </summary>
    /// <param name="slot"></param>
    /// <param name="item"></param>
    private void UpdateSlotInfo(InventorySlotUI slot, InventoryItem item) {
        // Basic info: name, weight and imag (if any).
        slot.gameObject.SetActive(true);
        slot.ItemNameText = item.ItemName;
        slot.WeightText = item.Weight + "";
        if (item.ItemImage != null) {
            slot.ItemImage.sprite = Sprite.Create(
                item.ItemImage, new Rect(0, 0, item.ItemImage.width, item.ItemImage.height), Vector2.one / 2f);
        }
        slot.InventoryItem = item;
        // Update the buttons' listeners (Remove and Sell).
        slot.RemoveItemButton.onClick.RemoveAllListeners();
        slot.RemoveItemButton.onClick.AddListener(delegate { RemoveItem(slot); });
        slot.SellItemButton.onClick.RemoveAllListeners();
        if (item is IValuable) {
            // If has a market value, then it can be sold.
            slot.SellItemButton.interactable = true;
            slot.SellItemButton.onClick.AddListener(delegate { SellItem(slot); });
        } else {
            // Otherwise, disable the sell button.
            slot.SellItemButton.interactable = false;
        }
        // Event to allow the user click over the slot.
        slot.OnPointerDownEvent = null;
        slot.OnPointerDownEvent += ItemClicked;
    }


    private void SellItem(InventorySlotUI slot) {
        if (slot.InventoryItem is InventoryWeapon) {
            // If the item sold is the weapon in use, remove it from the WeaponPanel.
            m_WeaponPanelUI.RemoveEquipedWeaponIfSlot(slot);
        }
        // If the item sold is drawn in InfoPanel, remove it.
        m_InfoItemPanel.RemoveDrawnItemIfSlot(slot);
        m_Inventory.SellItem(slot.InventoryItem);
        InstanceUpdateItems(m_Inventory.Items);
    }
    private void RemoveItem(InventorySlotUI slot) {
        if (slot.InventoryItem is InventoryWeapon) {
            // If the item removed is the weapon in use, remove it from the WeaponPanel.
            m_WeaponPanelUI.RemoveEquipedWeaponIfSlot(slot);
        }
        // If the item removed is drawn in InfoPanel, remove it.
        m_InfoItemPanel.RemoveDrawnItemIfSlot(slot);
        m_Inventory.RemoveItem(slot.InventoryItem);
        InstanceUpdateItems(m_Inventory.Items);
    }

    /// <summary>
    /// Method called when a slot is clicked. 1 click to show the info
    /// on the InfoPanel, 2 clicks to use the item.
    /// </summary>
    /// <param name="slot"></param>
    /// <param name="clicks"></param>
    private void ItemClicked(InventorySlotUI slot, int clicks) {
        if (clicks == 1) {
            m_InfoItemPanel.DrawItem(slot);
        } else if (clicks == 2) {
            // use item
            InventoryItem item = slot.InventoryItem;
            if (item is InventoryWeapon) {
                // Set on the WeaponPanel and equiped on the world.
                m_Inventory.UseItem(slot.InventoryItem);
                m_WeaponPanelUI.EquipWeapon(slot);
            } else if (item is InventoryConsumible) {
                // Used and removed from the inventory.
                m_Inventory.UseItem(slot.InventoryItem);
                RemoveItem(slot);
            }
        }
    }

}
