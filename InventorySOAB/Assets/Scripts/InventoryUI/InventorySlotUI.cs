using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using InventoryItems;

/// <summary>
/// Component for every slot in the items panel (inventory UI).
/// Inherits from IPointerClickHandler because a slot can be clicked.
/// </summary>
public class InventorySlotUI : MonoBehaviour, IPointerClickHandler {

    [SerializeField] private Text m_ItemNameText, m_WeightText;
    [SerializeField] private Image m_ItemImage;
    [SerializeField] private Button m_RemoveItemButton, m_SellItemButton, m_ItemButton;

    public string ItemNameText { get => m_ItemNameText.text; set => m_ItemNameText.text = value; }
    public string WeightText { get => m_WeightText.text; set => m_WeightText.text = value; }
    public Image ItemImage { get => m_ItemImage; set => m_ItemImage = value; }
    public Button SellItemButton { get => m_SellItemButton; }
    public Button RemoveItemButton { get => m_RemoveItemButton; }
    public Button ItemButton { get => m_ItemButton; }

    // Event called when the slot is clicked,
    // with the slot and the number of clicks made over the slot.
    public delegate void OnPointerDown(InventorySlotUI slot, int clicks);
    public OnPointerDown OnPointerDownEvent;


    private InventoryItem m_InventoryItem;
    public InventoryItem InventoryItem { get => m_InventoryItem; set => m_InventoryItem = value; }


    public void OnPointerClick(PointerEventData eventData) {
        OnPointerDownEvent(this, eventData.clickCount);
    }

}
