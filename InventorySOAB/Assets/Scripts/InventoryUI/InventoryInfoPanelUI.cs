using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Component of InfoPanel on the inventory 
/// to show the info from the slot selected.
/// </summary>
public class InventoryInfoPanelUI : MonoBehaviour {

    [SerializeField] private Text m_ItemNameText, m_ItemInfoText;
    [SerializeField] private Image m_ItemImagePanel;

    private InventorySlotUI m_Slot;
    private Color m_StartingColor;


    private void Awake() {
        m_StartingColor = m_ItemImagePanel.color;
    }


    public void DrawItem(InventorySlotUI slot) {
        m_ItemNameText.text = slot.ItemNameText;
        m_ItemInfoText.text = slot.InventoryItem.GetInfo();
        m_ItemImagePanel.sprite = slot.ItemImage.sprite;
        m_Slot = slot;
    }

    public void RemoveDrawnItem() {
        m_ItemNameText.text = "";
        m_ItemInfoText.text = "";
        m_ItemImagePanel.sprite = null;
        m_ItemImagePanel.color = m_StartingColor;
    }

    /// <summary>
    /// Removes the info only if it comes from the same slot given as parameter.
    /// </summary>
    /// <param name="slot"></param>
    public void RemoveDrawnItemIfSlot(InventorySlotUI slot) {
        if (slot.Equals(m_Slot)) {
            RemoveDrawnItem();
        }
    }

    public void UpdateSlotInfo() {
        if (m_Slot != null) {
            DrawItem(m_Slot);
        } else {
            RemoveDrawnItem();
        }
    }

}
