using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Linq;
using System;
using InventoryItems;

/// <summary>
/// Component for all pickable items.
/// </summary>
public class PickItem : MonoBehaviour {

    /**
     * With more time, it could be nice to create some custom editor
     * for each item type (with PropertyDrawer maybe), instead of having 
     * all the types as SerializeField and having to choose from an enum.
     * Also, if every item adds new info/functionality when picked, it 
     * could be useful to create a custom PickItemXXX class for each one.
     */

    public enum ItemType { 
        ResourceAmmo, 
        ConsumibleHealth, ConsumibleSpeed,
        WeaponPistol, WeaponSpear,
        ItemTrash
    }
    [SerializeField, Tooltip("Only the item type chosen on this list will be used")] 
    private ItemType m_Type;
    public ItemType Type { get => m_Type; set => m_Type = value; }

    [SerializeField] private ResourceAmmo m_ResourceAmmo;
    [SerializeField] private ConsumibleHealth m_ConsumibleHealth;
    [SerializeField] private ConsumibleSpeed m_ConsumibleSpeed;
    [SerializeField] private WeaponPistol m_WeaponPistol;
    [SerializeField] private WeaponSpear m_WeaponSpear;
    [SerializeField] private ItemTrash m_ItemTrash;

    public InventoryItem Item { 
        get {
            switch (m_Type) {
                case ItemType.ResourceAmmo:
                    return m_ResourceAmmo;
                case ItemType.ConsumibleHealth:
                    return m_ConsumibleHealth;
                case ItemType.ConsumibleSpeed:
                    return m_ConsumibleSpeed;
                case ItemType.WeaponPistol:
                    return m_WeaponPistol;
                case ItemType.WeaponSpear:
                    return m_WeaponSpear;
                default:
                    return m_ItemTrash;
            }
        }
    }


    public void PickUp() {
        Destroy(gameObject);
    }

}
