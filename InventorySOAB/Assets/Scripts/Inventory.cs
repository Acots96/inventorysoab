using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InventoryItems;

/// <summary>
/// Component attached to the character to manage all the items.
/// </summary>
public class Inventory : MonoBehaviour {

    [SerializeField] private float m_MaxWeight;
    [SerializeField] private Texture2D m_TrashImage;


    private List<InventoryItem> m_Items;
    public List<InventoryItem> Items { get => m_Items; }

    private float m_Weight, m_Money;
    public float Weight { get => m_Weight; }
    public float MaxWeight { get => m_MaxWeight; }
    public float Money { get => m_Money; }

    private Weapon m_WeaponInUse;
    public Weapon WeaponInUse { get => m_WeaponInUse; }

    private CharacterController m_CharacterController;


    private void Awake() {
        m_Items = new List<InventoryItem>();
        m_Weight = m_Money = 0;
        m_CharacterController = GetComponent<CharacterController>();
    }

    private void Start() {
        InventoryUI.Inventory = this;
    }


    /// <summary>
    /// Returns true if the item has been added to the inventory. Otherwise false.
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public bool AddItem(InventoryItem item) {
        // Cannot add the item if its weight would make exceed the inventory's limit.
        if (item.Weight + m_Weight > m_MaxWeight) {
            return false;
        }
        m_Items.Add(item);
        m_Weight += item.Weight;
        InventoryUI.UpdateItems(m_Items);
        //
        // If it's a weapon, it must be instantiated in the world
        // for the character to use it when selected in the inventory.
        if (item is InventoryWeapon) {
            InventoryWeapon wep = item as InventoryWeapon;
            wep.WeaponWorldGO =
                Instantiate(wep.WeaponPrefab, transform).GetComponent<Weapon>();
            if (wep is InventoryFireWeapon) {
                (wep.WeaponWorldGO as FireWeapon).ResourceNeeded 
                    = (wep as InventoryFireWeapon).ResourceNeeded;
            }
            wep.WeaponWorldGO.Enable(false);
        }
        return true;
    }

    /// <summary>
    /// Checks if the inventory has the resource needed for the weapon
    /// used by the character in that moment.
    /// </summary>
    /// <param name="spendIfTrue">If true, one unit of the resource will be spent. 
    /// Useful to use the weapon right after this method called.</param>
    /// <returns></returns>
    public bool CheckResourceForWeaponInUse(bool spendIfTrue = false) {
        if (m_WeaponInUse is MeleeWeapon) {
            // No resource needed.
            return true;
        }
        FireWeapon wep = m_WeaponInUse as FireWeapon;
        Type resourceType = wep.ResourcesTypeNeeded();
        if (resourceType == null) {
            // No resource needed.
            return true;
        }
        InventoryResource resource = FindItemByType(resourceType) as InventoryResource;
        if (resource != null && resource.ResourceAmount > 0) {
            // Resource found in the inventory with enough units.
            if (spendIfTrue) {
                resource.ResourceAmount--;
                if (resource.ResourceAmount <= 0) {
                    // No more units, so remove it.
                    RemoveItem(resource);
                }
            }
            InventoryUI.UpdateItems(Items);
            return true;
        }
        InventoryUI.UpdateItems(Items);
        return false;
    }

    public void ShowOrHide() {
        InventoryUI.ShowOrHide(m_Items);
    }


    public void EquipWeapon(InventoryWeapon wep) {
        if (m_WeaponInUse != null) {
            // Disable the actual weapon GO if any.
            m_WeaponInUse.Enable(false);
            m_WeaponInUse = null;
        }
        m_WeaponInUse = wep.WeaponWorldGO;
        m_WeaponInUse.Enable(true);
    }


    /// <summary>
    /// Finds all the items that are deteriorable and calls its method 
    /// DoDeterioration(). Completely deteriorated items are replaced in
    /// the inventory with an ItemTrash.
    /// </summary>
    public void NextDeteriorateIteration() {
        for (int i = 0; i < m_Items.Count; i++) {
            IDeteriorable deteriorable = m_Items[i] as IDeteriorable;
            if (deteriorable != null) {
                if (deteriorable.DoDeteriorate()) {
                    // Completely deteriorated -> becomes trash.
                    m_Items.RemoveAt(i);
                    ItemTrash trash = deteriorable.BecomeTrash();
                    trash.ItemImage = m_TrashImage;
                    m_Items.Add(trash);
                }
            }
        }
        InventoryUI.UpdateItems(m_Items);
    }



    private InventoryItem FindItemByType(Type type) {
        for (int i = 0; i < m_Items.Count; i++) {
            InventoryItem item = m_Items[i];
            if (type.Equals(item.GetType()) || type.IsAssignableFrom(item.GetType())) {
                // If the type is the same or is a subclass.
                return item;
            }
        }
        return null;
    }


    public void SellItem(InventoryItem item) {
        if (m_Items.Remove(item)) {
            // Item removed, so inventory's weight and money updated.
            m_Weight -= item.Weight;
            m_Money += (item as IValuable).GetValue();
            if (item is InventoryWeapon) {
                // If it is a weapon, destroy its world GO.
                InventoryWeapon wep = item as InventoryWeapon;
                Destroy(wep.WeaponWorldGO.gameObject);
            }
        }
    }
    public void RemoveItem(InventoryItem item) {
        // Same as SellItem, without increasing the money.
        if (m_Items.Remove(item)) {
            m_Weight -= item.Weight;
            if (item is InventoryWeapon) {
                // If is a wepoan must be destroyed in the world.
                InventoryWeapon wep = item as InventoryWeapon;
                Destroy(wep.WeaponWorldGO.gameObject);
            }
        }
    }

    public void UseItem(InventoryItem item) {
        if (item is InventoryWeapon) {
            // Weapon: equiped.
            EquipWeapon(item as InventoryWeapon);
        } else if (item is InventoryConsumible) {
            // Consumible: used and removed from inventory.
            (item as InventoryConsumible).DoConsume(m_CharacterController);
            RemoveItem(item);
        }
    }



    public void OnGUI() {
        if (GUI.Button(new Rect(10, 10, 150, 50), "Deteriorate Items")) {
            NextDeteriorateIteration();
        }
    }

}
