using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

/// <summary>
/// Component to control the character. It is a living actor,
/// so inherits from Actor class, manages the input and picks items.
/// </summary>
public class CharacterController : Actor {

    private Transform m_Transform;
    private Rigidbody m_Rigidbody;
    private Camera m_Camera;

    private Inventory m_Inventory;

    private Vector3 m_Movement;
    private Vector3 m_MousePositionInWorld;

    private float m_StartingSpeed;


    private void Awake() {
        m_Transform = transform;
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Camera = Camera.main;
        m_Inventory = GetComponent<Inventory>();
        m_StartingSpeed = m_MovementSpeed;
    }


    private void FixedUpdate() {
        // Move if there is movement input
        if (m_Movement != Vector3.zero) {
            Vector3 targetPosition = m_Rigidbody.position + m_Movement * m_MovementSpeed * Time.fixedDeltaTime;
            m_Rigidbody.MovePosition(targetPosition);
        }
        // Rotate towards mouse position in world
        if (m_MousePositionInWorld != Vector3.zero) {
            m_Transform.LookAt(m_MousePositionInWorld);
        }
    }


    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("PickItem")) {
            PickItem item = other.GetComponent<PickItem>();
            if (m_Inventory.AddItem(item.Item)) {
                item.PickUp();
            }
        }
    }


    public override void Heal(float health) {
        m_Health += health;
        m_Health = Mathf.Min(m_Health, 100f);
    }

    public override void TakeDamage(float damage) {
        m_Health -= damage;
        m_Health = Mathf.Max(m_Health, 0f);
        if (m_Health == 0) {
            Die();
        }
    }

    public override void ChangeSpeed(float incrementPercentage, float time = 0) {
        m_MovementSpeed *= incrementPercentage;
        if (time > 0) {
            // It uses Invoke after certain time to finish the effect,
            // but calls CancelInvoke to cancel the actual countdown of the same effect
            // to start the countdown again with the new amount of time.
            CancelInvoke(nameof(ChangeSpeedTime));
            Invoke(nameof(ChangeSpeedTime), time);
        }
    }

    private void ChangeSpeedTime() {
        m_MovementSpeed = m_StartingSpeed;
    }


    public void Die() {
        Debug.Log("DEAD!");
    }


    // ################# INPUT METHODS #################


    /// <summary>
    /// Gets the position of the mouse over the terrain 
    /// at character height (Y coordinate) using Raycast.
    /// </summary>
    /// <param name="ctx"></param>
    public void Rotate(InputAction.CallbackContext ctx) {
        // Mouse ignored if it is over a UI element
        // or the camera is destroyed (Quit game).
        if (EventSystem.current != null && EventSystem.current.IsPointerOverGameObject()
            || m_Camera == null) {
            return;
        }
        Vector2 screenPoint = ctx.ReadValue<Vector2>();
        Ray ray = m_Camera.ScreenPointToRay(screenPoint);
        Physics.Raycast(ray, out RaycastHit hit);
        m_MousePositionInWorld = hit.point;
        m_MousePositionInWorld.y = m_Transform.position.y;
    }

    /// <summary>
    /// Gets the input keys WASD
    /// </summary>
    /// <param name="ctx"></param>
    public void Move(InputAction.CallbackContext ctx) {
        m_Movement = ctx.ReadValue<Vector2>();
        m_Movement.z = m_Movement.y;
        m_Movement.y = 0;
    }

    /// <summary>
    /// Gets the input key I to show/hide the inventory.
    /// </summary>
    /// <param name="ctx"></param>
    public void Inventory(InputAction.CallbackContext ctx) {
        if (ctx.started) {
            m_Inventory.ShowOrHide();
        }
    }

    /// <summary>
    /// Gets mouse left click to use the weapon if any is
    /// equiped and can be used.
    /// Only if the mouse does not click over UI element.
    /// </summary>
    /// <param name="ctx"></param>
    public void Attack(InputAction.CallbackContext ctx) {
        // Mouse ignored if it is over a UI element.
        if (EventSystem.current != null && EventSystem.current.IsPointerOverGameObject()) {
            return;
        }
        if (ctx.started) {
            if (m_Inventory.WeaponInUse != null && m_Inventory.CheckResourceForWeaponInUse(true)) {
                m_Inventory.WeaponInUse.Use();
            }
        }
    }

}
