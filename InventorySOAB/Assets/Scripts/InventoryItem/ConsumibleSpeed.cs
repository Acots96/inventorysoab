using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventoryItems {

    /// <summary>
    /// SpeedIncrease as a consumible, so it inherits from InventoryConsumible.
    /// </summary>
    [System.Serializable]
    public class ConsumibleSpeed : InventoryConsumible {

        [SerializeField, Tooltip("Increase of %")] private float m_SpeedIncrease;
        [SerializeField] private float m_IncreaseTime;
        public float SpeedIncrease { get => m_SpeedIncrease; set => m_SpeedIncrease = value; }

        public ConsumibleSpeed(string name, float weight, float deteriorationAbleToWithstand,
            float deteriorationSpeed, float speedIncrease, float increaseTime, Texture2D image = null) :
            base(name, weight, deteriorationAbleToWithstand, deteriorationSpeed, image) {
            m_SpeedIncrease = speedIncrease;
            m_IncreaseTime = increaseTime;
        }


        public override void DoConsume(Actor actor) {
            actor.ChangeSpeed(m_SpeedIncrease, m_IncreaseTime);
            if (m_SpeedIncrease > 1) Debug.Log("SPEED INCREASED!");
            else if (m_SpeedIncrease < 1) Debug.Log("SPEED DECREASED!");
        }

    }

}
