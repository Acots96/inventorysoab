using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventoryItems {

    /// <summary>
    /// Class for the ammo. Inherits from InventoryResource because it is a resource.
    /// </summary>
    [System.Serializable]
    public class ResourceAmmo : InventoryResource {

        public ResourceAmmo(string name, float weight, float marketValue,
            float deteriorationAbleToWithstand, float deteriorationSpeed,
            int resourceAmount, Texture2D image = null) :
            base(name, weight, marketValue, deteriorationAbleToWithstand,
                deteriorationSpeed, resourceAmount, image) {
        }

    }

}
