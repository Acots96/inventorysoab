using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventoryItems {

    /// <summary>
    /// Health as a consumible, so it inherits from InventoryConsumible.
    /// </summary>
    [System.Serializable]
    public class ConsumibleHealth : InventoryConsumible {

        [SerializeField] private float m_HealthPoints;
        public float HealthPoints { get => m_HealthPoints; set => m_HealthPoints = value; }

        public ConsumibleHealth(string name, float weight, float deteriorationAbleToWithstand,
            float deteriorationSpeed, float healthPoints, Texture2D image = null) :
            base(name, weight, deteriorationAbleToWithstand, deteriorationSpeed, image) {
            m_HealthPoints = healthPoints;
        }


        public override void DoConsume(Actor actor) {
            actor.Heal(m_HealthPoints);
            Debug.Log("HEALED!");
        }

    }

}
