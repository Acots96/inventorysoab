using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InventoryItems;

namespace InventoryItems {

    /// <summary>
    /// Class that simulates the trash on the inventory.
    /// </summary>
    [System.Serializable]
    public class ItemTrash : InventoryItem {

        public ItemTrash(string name, float weight, Texture2D image = null) :
            base(name, weight, image) {

        }

    }

}
