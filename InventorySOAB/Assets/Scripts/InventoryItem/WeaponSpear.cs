using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventoryItems {

    /// <summary>
    /// Class for a spear in the inventory.
    /// Inherits from InventoryWeapon because it is a weapon.
    /// </summary>
    [System.Serializable]
    public class WeaponSpear : InventoryWeapon {

        public WeaponSpear(string name, float weight, float marketValue,
            float damagePerSecond,  GameObject weaponPrefab, Texture2D image = null) :
            base(name, weight, marketValue, damagePerSecond, weaponPrefab, image) {

        }

    }

}
