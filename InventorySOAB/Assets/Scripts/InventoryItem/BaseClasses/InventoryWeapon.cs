using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using System.Linq;

namespace InventoryItems {

    /// <summary>
    /// Class for all Weapons in the inventory.
    /// It also has a value, so implements IValuable inteface.
    /// </summary>
    public abstract class InventoryWeapon : InventoryItem, IValuable {

        [SerializeField] protected float m_MarketValue;
        [SerializeField] protected float m_DamagePerSecond;

        [SerializeField] protected GameObject m_WeaponPrefab;
        protected Weapon m_WeaponWorldGO;

        public float MarketValue { get => m_MarketValue; set => m_MarketValue = value; }
        public float DamagePerSecond { get => m_DamagePerSecond; set => m_DamagePerSecond = value; }

        public GameObject WeaponPrefab { get => m_WeaponPrefab; set => m_WeaponPrefab = value; }
        public Weapon WeaponWorldGO { get => m_WeaponWorldGO; set => m_WeaponWorldGO = value; }


        public InventoryWeapon(string name, float weight, float marketValue, float damagePerSecond,
            GameObject weaponPrefab, Texture2D image = null) : base(name, weight, image) {
            m_MarketValue = marketValue;
            m_DamagePerSecond = damagePerSecond;
            m_WeaponPrefab = weaponPrefab;
        }


        public float GetValue() {
            return m_MarketValue;
        }


        public override string GetInfo() {
            return base.GetInfo()
                + "\nMarketValue: " + m_MarketValue
                + "\nDamagePerSecond: " + m_DamagePerSecond;
        }

    }

}
