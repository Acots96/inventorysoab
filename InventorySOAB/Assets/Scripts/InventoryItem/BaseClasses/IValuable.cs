using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventoryItems {

    /// <summary>
    /// Interface used for every inventory item that has a market value.
    /// </summary>
    public interface IValuable {

        float GetValue();

    }

}
