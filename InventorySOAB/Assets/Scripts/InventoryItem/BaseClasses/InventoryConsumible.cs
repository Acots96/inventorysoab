using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventoryItems {

    /// <summary>
    /// Parent class for every item that is a consumible.
    /// All consumibles can be deteriorated, 
    /// so it also implements the interface IDeteriorable.
    /// </summary>
    public abstract class InventoryConsumible : InventoryItem, IDeteriorable {

        [SerializeField, Tooltip("Indicates the amount of deterioration able to withstand. The item is completely deteriorated when this attribute reaches 0")]
        protected float m_DeteriorationAbleToWithstand;
        [SerializeField, Tooltip("Amount of deterioration substracted each time")]
        protected float m_DeteriorationSpeed;
        public float DeteriorationAbleToWithstand { get => m_DeteriorationAbleToWithstand; set => m_DeteriorationAbleToWithstand = value; }
        public float DeteriorationSpeed { get => m_DeteriorationSpeed; set => m_DeteriorationSpeed = value; }


        public InventoryConsumible(string name, float weight, float deteriorationAbleToWithstand,
            float deteriorationSpeed, Texture2D image = null) : base(name, weight, image) {
            m_DeteriorationAbleToWithstand = deteriorationAbleToWithstand;
            m_DeteriorationSpeed = deteriorationSpeed;
        }


        public virtual bool DoDeteriorate() {
            m_DeteriorationAbleToWithstand -= m_DeteriorationSpeed;
            return m_DeteriorationAbleToWithstand <= 0;
        }

        public virtual ItemTrash BecomeTrash() {
            return new ItemTrash("Trash", m_Weight);
        }


        public override string GetInfo() {
            return base.GetInfo()
                + "\nDeterioration Level: " + m_DeteriorationAbleToWithstand
                + "\nDeterioration Speed: " + m_DeteriorationSpeed;
        }


        /// <summary>
        /// Every consumible item has to implement this method, 
        /// call when it is consumed.
        /// </summary>
        /// <param name="actor">Actor that consumes the item</param>
        public abstract void DoConsume(Actor actor);

    }

}
