using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventoryItems {

    /// <summary>
    /// Parent class for every item on the inventory.
    /// </summary>
    public abstract class InventoryItem {

        [SerializeField] protected string m_ItemName;
        [SerializeField] protected float m_Weight;
        [SerializeField] protected Texture2D m_ItemImage;

        public string ItemName { get => m_ItemName; set => m_ItemName = value; }
        public float Weight { get => m_Weight; set => m_Weight = value; }
        public Texture2D ItemImage { get => m_ItemImage; set => m_ItemImage = value; }


        public InventoryItem(string name, float weight, Texture2D itemImage = null) {
            m_ItemName = name;
            m_Weight = weight;
            ItemImage = itemImage;
        }


        public virtual string GetInfo() {
            return "Name: " + m_ItemName + "\nWeight: " + m_Weight;
        }

    }

}
