using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventoryItems {

    /// <summary>
    /// Interface used for all items that can be deteriorated.
    /// </summary>
    public interface IDeteriorable {

        float DeteriorationAbleToWithstand { get; set; }
        float DeteriorationSpeed { get; set; }

        // true = completely deteriorated
        bool DoDeteriorate();
        ItemTrash BecomeTrash();

    }

}
