using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using System.Linq;

namespace InventoryItems {

    /// <summary>
    /// Class for all fire weapons in the inventory, so inherits From InventoryWeapon.
    /// </summary>
    public abstract class InventoryFireWeapon : InventoryWeapon {

        [SerializeField, Tooltip("ClassName of the resource")] 
        protected string m_ResourceNeededName;

        private Type m_ResourceNeeded;
        public Type ResourceNeeded {
            get {
                if (m_ResourceNeeded == null)
                    m_ResourceNeeded = Type.GetType(m_ResourceNeededName);
                return m_ResourceNeeded;
            }
            set => m_ResourceNeeded = value;
        }


        public InventoryFireWeapon(string name, float weight, float marketValue, float damagePerSecond,
            Type resourceNeeded, GameObject weaponPrefab, Texture2D image = null) : 
            base(name, weight, marketValue, damagePerSecond, weaponPrefab, image) {
            m_MarketValue = marketValue;
            m_DamagePerSecond = damagePerSecond;
            ResourceNeeded = resourceNeeded;
            m_WeaponPrefab = weaponPrefab;
        }


        public override string GetInfo() {
            return base.GetInfo()
                + "\nResourceNeeded: " + m_ResourceNeeded;
        }

    }

}
