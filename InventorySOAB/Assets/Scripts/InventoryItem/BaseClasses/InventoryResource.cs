using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventoryItems {

    /// <summary>
    /// Parent class for every item that is a resource.
    /// All resources can be deteriorated and have a value, 
    /// so it also implements the interfaces IDeteriorable and IValuable.
    /// </summary>
    public abstract class InventoryResource : InventoryItem, IDeteriorable, IValuable {

        [SerializeField] protected float m_MarketValue;
        [SerializeField, Tooltip("Indicates the amount of deterioration able to withstand. The item is completely deteriorated when this attribute reaches 0")]
        protected float m_DeteriorationAbleToWithstand;
        [SerializeField, Tooltip("Amount of deterioration substracted each time")]
        protected float m_DeteriorationSpeed;
        [SerializeField] protected float m_ResourceAmount;

        public float MarketValue { get => m_MarketValue; set => m_MarketValue = value; }
        public float DeteriorationAbleToWithstand { get => m_DeteriorationAbleToWithstand; set => m_DeteriorationAbleToWithstand = value; }
        public float DeteriorationSpeed { get => m_DeteriorationSpeed; set => m_DeteriorationSpeed = value; }
        public float ResourceAmount { get => m_ResourceAmount; set => m_ResourceAmount = value; }


        public InventoryResource(string name, float weight, float marketValue, float deteriorationAbleToWithstand,
            float deteriorationSpeed, int resourceAmount, Texture2D image = null) : base(name, weight, image) {
            MarketValue = marketValue;
            m_DeteriorationAbleToWithstand = deteriorationAbleToWithstand;
            m_DeteriorationSpeed = deteriorationSpeed;
            ResourceAmount = resourceAmount;
        }


        public virtual bool DoDeteriorate() {
            m_DeteriorationAbleToWithstand -= m_DeteriorationSpeed;
            if (m_DeteriorationAbleToWithstand <= 0) {
                // Value is 0 when the item is completely deteriorated.
                m_MarketValue = 0;
                return true;
            } else {
                // Loses a 25% of value on each deterioration.
                m_MarketValue *= 0.75f;
                return false;
            }
        }

        public virtual ItemTrash BecomeTrash() {
            return new ItemTrash("Trash", m_Weight);
        }

        public float GetValue() {
            return m_MarketValue;
        }


        public override string GetInfo() {
            return base.GetInfo()
                + "\nMarket Value: " + m_MarketValue
                + "\nDeterioration Level: " + m_DeteriorationAbleToWithstand
                + "\nDeterioration Speed: " + m_DeteriorationSpeed
                + "\nResource Amount: " + m_ResourceAmount;
        }

    }

}
