using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventoryItems {

    /// <summary>
    /// Class for a pistol in the inventory. 
    /// Inherits from InventoryFireWeapon because it is a fire weapon.
    /// </summary>
    [System.Serializable]
    public class WeaponPistol : InventoryFireWeapon {

        public WeaponPistol(string name, float weight, float marketValue, float damagePerSecond,
            Type resourceNeeded, GameObject weaponPrefab, Texture2D image = null) :
            base(name, weight, marketValue, damagePerSecond, resourceNeeded, weaponPrefab, image) {

        }

    }

}
