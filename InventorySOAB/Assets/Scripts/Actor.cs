using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for all living actors.
/// </summary>
public abstract class Actor : MonoBehaviour {

    [SerializeField] protected float m_Health;
    [SerializeField] protected float m_MovementSpeed;

    public abstract void Heal(float health);
    public abstract void TakeDamage(float damage);

    /// <summary>
    /// Changes the moving speed of the actor. Multiplies the speed
    /// value by a certain percentage.
    /// </summary>
    /// <param name="incrementPercentage"></param>
    /// <param name="time">Time the effect will last. Leave it 0 for endless speed change.</param>
    public abstract void ChangeSpeed(float incrementPercentage, float time = 0);

}
