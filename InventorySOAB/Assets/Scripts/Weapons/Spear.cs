using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class for a Spear, which is a melee weapon, so inherits from MeleeWeapon.
/// </summary>
public class Spear : MeleeWeapon {

    protected override void Awake() {
        base.Awake();
    }

    protected override void Start() {
        base.Start();
    }


    protected override void Update() {
        base.Update();
    }


    protected override void UseEffect() {
        StartCoroutine(HitEffect());
    }

    // To simulate a spear hit, it first moves the GO forward with certain
    // time, and then moves it backwards with another amount of time.
    private IEnumerator HitEffect() {
        Vector3 startPosition = m_Transform.localPosition,
            targetPosition = m_Transform.localPosition + Vector3.forward * 1f;
        float progress = 0;
        // Forward
        while (progress <= 1) {
            progress += Time.deltaTime / m_HitTime;
            m_Transform.localPosition = Vector3.Lerp(startPosition, targetPosition, progress);
            yield return null;
        }
        // Backwards
        while (progress > 0) {
            progress -= Time.deltaTime / m_RecoverTime;
            m_Transform.localPosition = Vector3.Lerp(startPosition, targetPosition, progress);
            yield return null;
        }
        m_CanHit = true;
    }

}
