using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class for melee weapons such a spear.
/// </summary>
public abstract class MeleeWeapon : Weapon {

    [SerializeField] protected float m_HitTime, m_RecoverTime;

    protected Transform m_Transform;

    protected bool m_CanHit;


    protected override void Awake() {
        m_CanHit = true;
        m_Transform = transform;
    }

    protected override void Start() {
        
    }


    protected override void Update() {
        
    }


    public override void Use() {
        if (!m_CanHit) {
            return;
        }
        m_CanHit = false;
        UseEffect();
    }

    protected abstract void UseEffect();

}
