using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for every weapon on the world.
/// </summary>
public abstract class Weapon : MonoBehaviour {

    protected abstract void Awake();
    protected abstract void Start();
    protected abstract void Update();

    public virtual void Enable(bool enable) {
        gameObject.SetActive(enable);
    }

    public abstract void Use();

}
