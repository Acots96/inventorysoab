using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InventoryItems;

/// <summary>
/// Class for fire weapons such a pistol
/// </summary>
public abstract class FireWeapon : Weapon {

    // World position for the bullet to be enabled.
    [SerializeField] protected Transform m_ThrowPoint;
    [SerializeField] protected float m_FireRate;
    [SerializeField] protected GameObject m_BulletPrefab;

    protected Type m_ResourceNeeded;
    public Type ResourceNeeded { get => m_ResourceNeeded; set => m_ResourceNeeded = value; }

    private float m_DeltaTime;
    private bool m_CanFire;
    private List<Bullet> m_BulletsInUsePool, m_BulletsNotInUsePool;


    protected override void Awake() {
        m_DeltaTime = 0;
        m_CanFire = true;
        // To avoid constant instantiation, which is not efficient, 
        // it uses a pool for the bullets.
        m_BulletsInUsePool = new List<Bullet>();
        m_BulletsNotInUsePool = new List<Bullet>();
        for (int i = 0; i < 20; i++) {
            m_BulletsNotInUsePool.Add(CreateBullet());
        }
    }

    protected override void Start() {

    }


    protected virtual Bullet CreateBullet() {
        GameObject go = Instantiate(m_BulletPrefab, m_ThrowPoint);
        go.SetActive(false);
        Bullet b = go.GetComponent<Bullet>();
        b.m_OnExplodedEvent += BulletExploded;
        return b;
    }


    protected override void Update() {
        if (!m_CanFire) {
            // Fire weapons have a certain fire rate,
            // which is calculated here.
            m_DeltaTime += Time.deltaTime;
            if (m_DeltaTime >= m_FireRate) {
                m_CanFire = true;
                m_DeltaTime = 0;
            }
        }
    }


    public override void Use() {
        if (!m_CanFire) {
            return;
        }
        m_CanFire = false;
        ThrowBullet();
    }

    public Type ResourcesTypeNeeded() {
        return m_ResourceNeeded;
    }


    protected void ThrowBullet() {
        if (m_BulletsNotInUsePool.Count == 0) {
            // Uses the pool to get a disabled bullet, but if it is empty
            // then it must create a new bullet.
            m_BulletsNotInUsePool.Add(CreateBullet());
        }
        // Gets an unused bullet, removes it from the disabled list
        // and enables it to be thrown.
        Bullet bullet = m_BulletsNotInUsePool[0];
        m_BulletsNotInUsePool.RemoveAt(0);
        m_BulletsInUsePool.Add(bullet);
        //
        Transform tr = bullet.transform;
        tr.parent = null;
        tr.up = m_ThrowPoint.forward;
        tr.position = m_ThrowPoint.position;
        bullet.gameObject.SetActive(true);
        bullet.Throw();
    }

    protected void BulletExploded(Bullet b) {
        b.gameObject.SetActive(false);
        b.transform.parent = m_ThrowPoint;
        // When the bullet explodes, it goes back to the pool.
        m_BulletsInUsePool.Remove(b);
        m_BulletsNotInUsePool.Add(b);
    }

}
