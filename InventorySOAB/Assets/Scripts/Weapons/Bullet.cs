using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Component for a world bullet.
/// </summary>
public class Bullet : MonoBehaviour {

    [SerializeField] private float m_BulletSpeed;

    private Transform m_Transform;
    private Rigidbody m_Rigidbody;

    // Event implementable from outside, called when the bullet explodes.
    public delegate void OnExploded(Bullet b);
    public OnExploded m_OnExplodedEvent;
    public OnExploded OnExplodedEvent { get => m_OnExplodedEvent; }


    private void Awake() {
        m_Transform = transform;
        m_Rigidbody = GetComponent<Rigidbody>();
    }


    public void Throw() {
        m_Rigidbody.velocity = m_Transform.up * m_BulletSpeed;
    }


    private void OnCollisionEnter(Collision collision) {
        m_OnExplodedEvent(this);
    }

}
