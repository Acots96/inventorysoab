using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class for a Pistol, which is a fire weapon, so inherits from FireWeapon.
/// </summary>
public class Pistol : FireWeapon {

    protected override void Awake() {
        base.Awake();
    }


    protected override void Update() {
        base.Update();
    }

}
