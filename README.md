# SonsOfABit test - Inventory by Aleix Cots Molina

This little project made with Unity (version 2021.1.10f1) contains a simple inventory which can have different types of items (weapons, consumibles, resources and trash).
To do that, I have created a little top-down game with a static camera, a character controlled by the user with an inventory, some prefabs simulating the different items of the game, a visual interface to manage the items on the inventory and a group of classes using inheritance for a better code architecture, readability and scalability.

*(Read the comments in the scripts to know more about each one of them)*

## Character control 

To allow the user control the character I have created a GameObject with a CharacterController script and an Inventory script which holds all the items:
 - Actor: Base component for any character with health and speed.
  - CharacterController: Inherits from Actor. Component to control the character.
 - Inventory: Component to manage the items in the inventory. It also has the total amounts of weight and money.
 
## Prefabs and world objects

To allow the character pick different items, I have created a PickItem script which every pickable GameObject must have. This script has an enum with all the types that can be in the inventory and a reference to each one of them (ResourceAmmo, ConsumibleHealth, ConsumibleSpeed, WeaponPistol, WeaponSpear and ItemTrash). 
This is a bit ugly, because the user has to update the enum and add a new reference on the script every time that a new inventory item (if inherits from any InventoryXXX abstract class) is added to the project. In order to make it more developer-friendly, UnityEditor and PropertyDrawer could be used to replace the enum with a dynamic dropdown array of strings (class names) and make a custom inspector to show only the class selected in the dropdown.
Finally, an item can be picked only if its weight plus the weight of the inventory is less than the maximum weight allowed *(item.weight + inventory.weight <= inventory.maxWeight)*.

There are two types of weapons: melees and fire weapons. Only the second type uses resources.
Moreover, prefabs of pickable weapons have the weapon itself (GameObject) as child, so the Inventory can instantiate the GameObject weapon as child of the Character.
The classes used for GameObject weapons are:
 - Weapon: Abstract base component for GameObjects weapons. Has the abstract method Use, to be implemented in all subclasses, and the method Enable to activate/deactivate the GameObject.
  - FireWeapon: Inherits from Weapon. Abstract base component for all fire weapons. Manages the pool of bullets.
   - Pistol: Inherits from FireWeapon. Component for a pistol.
  - MeleeWeapon: Inherits from Weapon. Abstract base component for all melee weapons. Has the abstract method of UseEffect in order to allow all subclasses have their on use effect.
   - Spear: Inherits from MeleeWeapon. Component for a spear. Overrides the UseEffect method to simulate the attack of a spear.
   
Finally, the bullet GameObject used by the fire weapon is a simple prefab with a Bullet script to manage it. In order to avoid the contact with the fire weapon itself, the bullet and the pistol GameObjects with colliders have their own layers and they are unchecked in the physics matrix.
 
## Inventory items
 
Items in the inventory are not world objects, but simple objects ordered with inheritance. All the actions relative to them are managed on the Inventory component.

First of all, these are the classes used for items in the inventory (intended to be used as base classes, so they are abstract), which are simple objects instead of Monobehaviours. They have the following structure:
 - InventoryItem: Base class for all items. With name, weight and image (optional). 
  - InventoryWeapon: Base class for all weapons. With market value and damage per second.
   - InventoryFireWeapon: Base class for all fire weapons. The aim of this class is to hold the attribute ResourceNeeded, used only by fire weapons.
  - InventoryConsumible: Base class for all consumibles. With deterioration able to withstand and deterioration speed. Has the abstract method DoConsume which must be implemented by all subclasses to have their own behaviour.
  - InventoryResource: Base class for all resources. With market value, resource amount, eterioration able to withstand and deterioration speed.
  - ItemTrash: Trash is always the same, so no need to create an abstract base class for many types of Trash, one is enough.

To complement those base classes I have created some interfaces:
 - IValuable: interface used for all the items that have a market value, like weapons and resources, in order to have a common method to get the market value regardless of the item type.
 - IDeteriorable: interface used for all the items that can deteriorate, like resources and consumibles, in order to have a common method to deteriorate the item and make it trash regardless of the item type.
 
Now, to have real items that inherit from the abstract classes mentioned above, I have created these classes:
 - WeaponPistol: Inherits from InventoryFireWeapon.
 - WeaponSpear: Inherits from InventoryWeapon.
 - ConsumibleSpeed: Inherits from InventoryConsumible. Overrides DoConsume to apply the speed change to the character.
 - ConsumibleHealth: Inherits from InventoryConsumible. Overrides DoConsume to heal the character.
 - ResourceAmmo: Inherits from InventoryResource.

## Inventory visual interface

To allow the user manage the items of the inventory and check their information, I have created an InventoryPanel in the Canvas which has 4 panels inside: 
 - WeaponInUsePanel: Shows the information of the weapon in use (name, damage per second and image). Class -> InventoryWeaponPanelUI.
 - InfoItemPanel: Shows the information of the item clicked (all its attributes, the name and the image). Class -> InventoryInfoPanelUI.
 - CentralPanel: Shows the total weight and money. Class -> None.
 - ItemsPanel: Dynamic ScrollView which shows all the items on the inventory. Uses the prefab Slot for all the items shown. Class -> InventoryUI.
 
Classes for UI elements:
 - InventoryUI: Main class of the inventory visual interface. Manages the total weight and money, the panel with the items (ItemsPanel) and the available actions of these items (sell, remove, show information and use).
 - InventoryWeaponPanelUI: Class which has the information of the weapon in use.
 - InventoryInfoPanelUI: Class which shows the information of the item clicked.
 - InventorySlotUI: Class used by all the slots of the inventory. Shows the basic information of an item and implements the IPointerClickHandler interface to handle a click over it.  
 
Furthermore, to make the items deteriorate another level, There is a button 

# Actions

Character:
 - Movement: WASD.
 - Rotation: Moves the mouse over the screen, the character will rotate towards it.
 - Attack: Left button of the mouse.

Inventory:
 - Open/Close: I.
 - Show item's information: Left mouse click over the slot on the inventory.
 - Equipar arma/Usar consumible: Deouble left mouse click over the slot on the inventory.
 - Sell item: Click over the sell button on the item's slot (credit card over a hand).
 - Remove item: Click over the remove button on the item's slot (bin).
 
## Resources

All the images in the sprites folder are from (free source) https://www.iconsdb.com/


